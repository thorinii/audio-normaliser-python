import sys

import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import soundfile as sf


NOISE_FLOOR = -65
TARGET_DB = -0.2


def main(args):
    in_file = args[0]
    # TODO: craft automatically if not set
    out_file = args[1]

    signal, sample_rate = sf.read(in_file, dtype='float32')
    print(f'{len(signal)} samples')

    compressed_signal = compress(signal, sample_rate)

    print('writing out...')
    sf.write(out_file, compressed_signal, sample_rate)


def compress(signal, sample_rate):
    original_rms = calculate_rms(signal, 1024)

    # how long (in chunks, from seconds) to smooth for
    LONG_LENGTH = int(7 * sample_rate / 1024)
    SHORT_LENGTH = int(1.8 * sample_rate / 1024)

    rms = original_rms
    average_level = np.mean(rms)

    print('smoothing long-range', LONG_LENGTH)
    long_range = smooth_down(rms, LONG_LENGTH, average_level)
    long_range = expand_interpolate(long_range, LONG_LENGTH)
    long_range = long_range[0:len(rms)]
    average_level = np.mean(long_range)
    rms = rms + (average_level - long_range)

    print('smoothing short-range', SHORT_LENGTH)
    short_range = smooth_down(rms, SHORT_LENGTH, average_level)
    short_range = expand_interpolate(short_range, SHORT_LENGTH)
    short_range = short_range[0:len(rms)]
    average_level = np.mean(short_range)
    rms = rms + (average_level - short_range) * 0.3

    print('applying compression...')
    diff = rms - original_rms
    multiplier = np.power(10, diff / 20)
    multiplier_interp = expand_interpolate(multiplier, 1024)
    multiplier_interp = multiplier_interp[0:len(signal)]
    compressed_raw = signal * multiplier_interp

    print('normalising...')
    peak = max(np.max(compressed_raw), -np.min(compressed_raw))
    target_peak = 10 ** (TARGET_DB / 20)
    compressed = compressed_raw * (target_peak / peak)

    return compressed


def calculate_rms(amplitude, chunk_size):
    chunks = np.array_split(amplitude, math.ceil(len(amplitude) / chunk_size))
    rms_by_chunk = [np.sqrt(np.mean(c ** 2)) for c in chunks]
    rms_by_chunk = 20 * np.log10(rms_by_chunk)
    return rms_by_chunk


def smooth_down(values, window_size, default_level):
    half_range = int(window_size / 2)
    count = max(2, int(math.ceil(len(values) / window_size)))

    array = []
    for i in range(0, count):
        j = i * window_size
        lower_edge = max(0, j - half_range)
        higher_edge = min(len(values) - 1, j + half_range)

        slice = values[lower_edge:higher_edge]
        slice = [v for v in slice if v > NOISE_FLOOR]

        if len(slice) == 0:
            if array:
                array.append(array[i - 1])
            else:
                array.append(default_level)
        else:
            array.append(np.percentile(slice, 90))

    return np.array(array)


def expand_interpolate(values, factor):
    x = np.arange(0, len(values))
    new_x = np.arange(0, len(values), 1 / factor)
    return np.interp(new_x, x, values)

def clamp_to_noise_floor(array):
    for i in range(0, len(array)):
        if array[i] <= NOISE_FLOOR:
            array[i] = NOISE_FLOOR
    return array


if __name__ == '__main__':
    main(sys.argv[1:])
